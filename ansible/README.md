# Online validator
https://ansible.sivel.net/test/

# Variables

### Required
- `NGINX_SERVER_NAME`: `vvdev.ru`
- `NGINX_CONTAINER_PORT`: `80`

### Optional
- `ENABLE_SSL`: `boolean` 
- `ENABLE_INDEXING`: `boolean` 
- `NGINX_PROXY_API`: `url` 
- `NGINX_ADDITIONAL`: `string` 

# Important
Важно при включении `SSL`: \
    - Сначала на сервере в ручном режиме сгенерировать `SSL` сертификат (через `certbot`) \
    - Потом произвести деплой проекта с переменной `ENABLE_SSL` `true`

# Example 
```yaml
NGINX_ADDITIONAL: |
    location /test {
        proxy_pass http://localhost:9999;
    }
```
